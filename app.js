$(document).ready(function() {

    // Componente de menú con barra lateral
    $("#nav-toggle-button").click(function(e) {
        $(".nav__sidebar").toggleClass('active');
        $(".nav__button").toggleClass('pushed');
        // Cambiar el nombre de las clases de FontAwesome si se desea proyectar otro tipo de icono para el botón
        if ($(".nav__button").hasClass('pushed')) {
            $(".nav__icon").removeClass('fa-bars').addClass('fa-times')
        } else {
            $(".nav__icon").removeClass('fa-times').addClass('fa-bars')
        }
    })
})